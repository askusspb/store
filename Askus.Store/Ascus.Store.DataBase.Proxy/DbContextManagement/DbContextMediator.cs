﻿
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.DbContextManagement
{
    public sealed class DbContextMediator : IDisposable
    {
        private Lazy<DebugStorageEntities> _dbContextLazy;
        public DbContextMediator()
        {
            _dbContextLazy = new Lazy<DebugStorageEntities>(
                () => 
                {
                    var context = new DebugStorageEntities();
                    context.Configuration.ProxyCreationEnabled = false;
                    return context;
                });
            Debug.WriteLine("Creating Db context holder " + this.GetHashCode());
        }

        public DebugStorageEntities DbContext => _dbContextLazy.Value;

        public void Dispose()
        {
            if (_dbContextLazy.IsValueCreated && _dbContextLazy.Value != null)
            {
                _dbContextLazy.Value.Dispose();
            }
            Debug.WriteLine("Disposing Db context holder " + this.GetHashCode());
        }
    }
}
