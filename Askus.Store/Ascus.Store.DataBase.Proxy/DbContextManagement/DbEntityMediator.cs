﻿using Askus.Store.DataBase.Proxy.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.DbContextManagement
{
    public sealed class DbEntityMediator
    {
        private readonly DbContextMediator _contextMediator;

        public DbEntityMediator(DbContextMediator context)
        {
            _contextMediator = context;
        }

        public IQueryable<T> GetTable<T>()
            where T : class, IEntity
        {
            return _contextMediator.DbContext.Set<T>();
        }

        public async Task<T> SaveEntity<T>(T entity)
            where T : class, IEntity
        {

            if (entity.Id == 0)
                _contextMediator.DbContext.Set<T>().Add(entity);
            else
            {
                var existItem = _contextMediator.DbContext.Set<T>().Find(entity.Id);
                _contextMediator.DbContext.Entry(existItem).CurrentValues.SetValues(entity);
            }
               
               // _contextMediator.DbContext.Entry(entity).State = System.Data.Entity.EntityState.Modified;

            await _contextMediator.DbContext.SaveChangesAsync();
            return entity;
        }

        public Task<int> RemoveEntity<T>(T entity)
             where T : class, IEntity
        {
            _contextMediator.DbContext.Set<T>().Remove(entity);
            return _contextMediator.DbContext.SaveChangesAsync();
        }
    }
}
