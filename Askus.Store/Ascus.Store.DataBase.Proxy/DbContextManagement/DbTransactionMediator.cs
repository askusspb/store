﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.DbContextManagement
{
    public sealed class DbTransactionMediator
    {
        private readonly DbContextMediator _dbContext;
        private DbContextTransaction _currentTransaction;

        public DbTransactionMediator(DbContextMediator cntx)
        {
            _dbContext = cntx;
        }

        public void OpenTransaction()
        {
            if (_currentTransaction == null)
                _currentTransaction = _dbContext.DbContext.Database.BeginTransaction();
        }

        public void CommitTranaction()
        {
            try
            {
                _currentTransaction.Commit();
            }
            catch
            {
                _currentTransaction.Rollback();
                throw;
            }
            finally
            {
                _currentTransaction = null;
            }

        }

        public void Rollback()
        {
            _currentTransaction?.Rollback();
            _currentTransaction = null;
        }
    }
}
