﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.Interface
{
    /// <summary>
    /// Интерфейс всех хранимых доменных моделей
    /// </summary>
    public interface IDomainEntity : IEntity
    {
        DateTimeOffset CreatedDate { get; set; }
        DateTimeOffset? LastUpdatedDateTime { get; set; }
       // DateTime? DeletedDateTime { get; set; }
        int CreatedUserId { get; set; }
        int? UpdatedUserId { get; set; }

    }
}
