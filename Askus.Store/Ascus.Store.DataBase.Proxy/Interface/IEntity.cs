﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.Interface
{
    /// <summary>
    /// Интерфейс всех хранимых сущностей
    /// </summary>
    public interface IEntity
    {
        int Id { get; set; }
    }
}
