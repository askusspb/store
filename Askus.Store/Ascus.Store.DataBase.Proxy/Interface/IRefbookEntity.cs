﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.DataBase.Proxy.Interface
{
    /// <summary>
    /// Интерфейс всех хранимых справочников
    /// </summary>
    public interface IRefbookEntity : IEntity
    {
        string Aka { get; set; }
        string Title { get; set; }
    }
}
