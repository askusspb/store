//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Askus.Store.DataBase.Proxy
{
    using System;
    using System.Collections.Generic;
    
    public partial class Page
    {
        public int Id { get; set; }
        public System.DateTimeOffset CreatedDate { get; set; }
        public Nullable<System.DateTimeOffset> LastUpdatedDateTime { get; set; }
        public int CreatedUserId { get; set; }
        public Nullable<int> UpdatedUserId { get; set; }
        public string Title { get; set; }
        public Nullable<int> SectionId { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string Key { get; set; }
        public bool IsPublished { get; set; }
    
        public virtual User User { get; set; }
        public virtual User User1 { get; set; }
    }
}
