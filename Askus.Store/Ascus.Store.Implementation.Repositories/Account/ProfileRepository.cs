﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomItem = Askus.Store.Infrastructure.Domain.Account.Profile;
using DbItem = Askus.Store.DataBase.Proxy.Profile;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Application;

namespace Askus.Store.Implementation.Repositories.Account
{
    public class ProfileRepository : DomainRepository<DomItem, DbItem>
    {
        public ProfileRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext, userContext)
        {
        }
    }
}
