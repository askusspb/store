﻿using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Implementation.Repositories.Mapping;
using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using System.Linq;
using System.Threading.Tasks;
using DbItem = Askus.Store.DataBase.Proxy.User;
using DomItem = Askus.Store.Infrastructure.Domain.Account.User;

namespace Askus.Store.Implementation.Repositories.Account
{
    public class UserRepository : DomainRepository<DomItem, DbItem>, IUserRepository
    {
        public UserRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext, userContext)
        {
        }

        #region IUserRepository
        public async Task<DomItem> GetUserByLoginAsync(string login)
        {
            var dbUser = await Task.Run(() => GetAllQuery().Where(a => a.Login == login) .FirstOrDefault());

            return (dbUser as DbItem).Map<DomItem>();
        }
        #endregion

    }
}
