﻿using Askus.Store.DataBase.Proxy;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.DataBase.Proxy.Interface;
using Askus.Store.Infrastructure.Interface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository
         where TEntity : class, IEntity
    {
        private readonly DbEntityMediator _dbContextHolder;

        public BaseRepository(DbEntityMediator dbContext)
        {
            _dbContextHolder = dbContext;
        }

        protected IQueryable<TEntity> GetAllQuery()
        {
            return _dbContextHolder.GetTable<TEntity>();
        }

        protected Task<TEntity> SaveEntityAsync(TEntity entity)
        {
            return _dbContextHolder.SaveEntity(entity);
        }

        protected Task RemoveEntityAsync(TEntity entity)
        {
            return _dbContextHolder.RemoveEntity(entity);
        }
    }
}
