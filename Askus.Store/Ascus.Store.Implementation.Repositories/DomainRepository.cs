﻿using Askus.Store.DataBase.Proxy;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.DataBase.Proxy.Interface;
using Askus.Store.Implementation.Repositories.Mapping;
using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Repositories
{
    public abstract class DomainRepository<TDomain, TEntity> : BaseRepository<TEntity>, IDomainRepository<TDomain>, IStorageRepository
        where TDomain : BaseDomain, new()
        where TEntity : class, IEntity, new()
    {

        private readonly IUserContext _userContext;

        #region ctor

        public DomainRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext)
        {
            _userContext = userContext;
        }

        #endregion

        #region IDomainRepository
        public virtual Task<IList<TDomain>> GetAllAsync()
        {
            return Task.Run(() => (IList<TDomain>)GetAllQuery().Map<TDomain, TEntity>().ToList());
        }

        public Task<TDomain> GetByIdAsync(int id)
        {
            return Task.Run(() => GetAllQuery().SingleOrDefault(a => a.Id == id).Map<TDomain>());
        }


        public virtual Task RemoveAsync(TDomain model)
        {
            var entity = GetAllQuery().SingleOrDefault(a => a.Id == model.Id);
            return RemoveEntityAsync(entity);
        }


        public virtual async Task<TDomain> SaveAsync(TDomain model)
        {
            var entity = model.Map<TEntity>();

            if (entity is IDomainEntity)
            {
                if (entity.Id == 0)
                {
                    (entity as IDomainEntity).CreatedDate = DateTimeOffset.Now;
                    (entity as IDomainEntity).CreatedUserId = _userContext.CurrentUser.Id;
                }
                else
                {
                    var existItem = GetAllQuery().First(a => a.Id == entity.Id);

                    (entity as IDomainEntity).LastUpdatedDateTime = DateTimeOffset.Now;
                    (entity as IDomainEntity).UpdatedUserId = _userContext.CurrentUser.Id;
                    (entity as IDomainEntity).CreatedDate = (existItem as IDomainEntity).CreatedDate;
                    (entity as IDomainEntity).CreatedUserId = (existItem as IDomainEntity).CreatedUserId;
                }
            }

            entity = await  SaveEntityAsync(entity);
            return entity.Map<TDomain>();
        }

        public Task<GridVM<TDomain, TFilter>> GetFilteredAsync<TFilter>(TFilter filter)
            where TFilter : FilterVM
        {
            var query = BuildDefaultQuery(filter);
            query = ApplyFilter(query, filter).OrderBy(a => a.Id);
            var totalCount = query.Count();

            if (filter.Skip != null)
                query = query.Skip(filter.Skip.Value);
            if (filter.Take != null)
                query = query.Take(filter.Take.Value);

            return Task.Run(() => new GridVM<TDomain, TFilter>() {
                Items = query.Map<TDomain, TEntity>().ToList(),
                Filter = filter,
                Total = totalCount,
            });
        }

        #endregion

        #region IStorageRepository

        public Task SaveAsync(BaseDomain item)
        {
            if (item is TDomain)
            {
                return this.SaveAsync(item as TDomain);
            }
            throw new InnerStoreException("Invalid domain type");
        }


        public Task RemoveAsync(BaseDomain item)
        {
            if (item is TDomain)
            {
                return this.RemoveAsync(item as TDomain);
            }
            throw new InnerStoreException("Invalid domain type");
        }
        #endregion

        #region Protected

        protected virtual IQueryable<TEntity> ApplyFilter<TFilter>(IQueryable<TEntity> src, TFilter filter)
             where TFilter : FilterVM
        {
            return src;
        }
        #endregion

        #region private

        private IQueryable<TEntity> BuildDefaultQuery<TFilter>(TFilter filter)
             where TFilter : FilterVM
        {
            var query = GetAllQuery();
            if (filter.Id != null)
                query = query.Where(a => a.Id == filter.Id);
            return query;
        }

        #endregion
    }
}
