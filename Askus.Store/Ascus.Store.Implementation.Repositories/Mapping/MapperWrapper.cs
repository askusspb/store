﻿using Askus.Store.DataBase.Proxy;
using Askus.Store.DataBase.Proxy.Interface;
using Askus.Store.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Repositories.Mapping
{
    public static class MapperWrapper
    {
        public static TModel Map<TModel>(this IEntity entity)
            where TModel: class, new()
        {
            if (entity == null)
                return null;

            var mapper = EmitMapper
                .ObjectMapperManager
                .DefaultInstance
                .GetMapperImpl(entity.GetType(), typeof(TModel), MappingConfiguration.GetMappingConfiguration());

            return mapper.Map(entity) as TModel;
        }

        public static TEntity Map<TEntity>(this BaseDomain domain)
            where TEntity : class, IEntity, new()
        {
            if (domain == null)
                return null;

            return EmitMapper
                .ObjectMapperManager
                .DefaultInstance
                .GetMapperImpl(domain.GetType(), typeof(TEntity), MappingConfiguration.GetMappingConfiguration())
                .Map(domain) as TEntity;
        }

        public static IEnumerable<TModel> Map<TModel, TEntity>(this IQueryable<TEntity> src)
            where TModel : new()
            where TEntity : IEntity
        {

            if (src == null)
                return null;

            var mapper = EmitMapper.ObjectMapperManager.DefaultInstance.GetMapper<TEntity, TModel>(MappingConfiguration.GetMappingConfiguration());
            return src.ToList().Select(a => mapper.Map(a));
        }
    }
}
