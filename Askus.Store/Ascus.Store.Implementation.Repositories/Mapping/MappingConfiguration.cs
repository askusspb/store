﻿using EmitMapper.MappingConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Repositories.Mapping
{
    public static class MappingConfiguration
    {
        private static DefaultMapConfig _defaultConfig;

        public static DefaultMapConfig GetMappingConfiguration()
        {
            return _defaultConfig;
        }

        static MappingConfiguration()
        {
            _defaultConfig = new DefaultMapConfig();
            ToDbConfig(_defaultConfig);
            ToDtoConfig(_defaultConfig);
            ViewModelConfig(_defaultConfig);
        }

        private static void ToDbConfig(DefaultMapConfig config)
        {

        }

        private static void ToDtoConfig(DefaultMapConfig config)
        {

        }

        private static void ViewModelConfig(DefaultMapConfig config)
        {

        }
    }
}
