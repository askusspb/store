﻿using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Implementation.Repositories.Mapping;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using DbItem = Askus.Store.DataBase.Proxy.Article;
using DomItem = Askus.Store.Infrastructure.Domain.Publication.Article;

namespace Askus.Store.Implementation.Repositories.Publication
{
    public class ArticleRepository : DomainRepository<DomItem, DbItem>, IArticleRepository
    {

        public ArticleRepository(DbEntityMediator dbContext, IUserContext usrCntx) : base(dbContext, usrCntx)
        {
        }

        public Task<DomItem> GetArticleByAliasAsyc(string alias)
        {
            return Task.Run(() => GetAllQuery().Where(a => a.Alias == alias).Map<DomItem, DbItem>().FirstOrDefault());
        }
    }
}
