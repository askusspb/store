﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Implementation.Repositories.Mapping;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using DbItem = Askus.Store.DataBase.Proxy.Page;
using DomItem = Askus.Store.Infrastructure.Domain.Publication.Page;

namespace Askus.Store.Implementation.Repositories.Publication
{
    public class PageRepository : DomainRepository<DomItem, DbItem>, IPageRepository
    {
        public PageRepository(DbEntityMediator dbContext, IUserContext usrCntx) : base(dbContext, usrCntx)
        {
        }

        #region IPageRepository

        public IList<DomItem> GetPublished()
        {
            return GetAllQuery()
                .Where(a => a.IsPublished)
                .Map<DomItem,DbItem>()
                .ToList();
        }

        #endregion
    }
}
