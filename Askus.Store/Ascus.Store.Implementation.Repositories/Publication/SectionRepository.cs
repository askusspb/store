﻿using Askus.Store.DataBase.Proxy;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Implementation.Repositories.Mapping;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using DbItem = Askus.Store.DataBase.Proxy.Section;
using DomItem = Askus.Store.Infrastructure.Domain.Publication.Section;

namespace Askus.Store.Implementation.Repositories.Publication
{
    public class SectionRepository : DomainRepository<DomItem, DbItem>, ISectionRepository
    {
        public SectionRepository(DbEntityMediator dbContext, IUserContext usrCntx) : base(dbContext, usrCntx)
        {
        }

        public IList<DomItem> GetPublished()
        {
            return GetAllQuery()
                .Map<DomItem, DbItem>()
                .ToList();
        }
    }
}
