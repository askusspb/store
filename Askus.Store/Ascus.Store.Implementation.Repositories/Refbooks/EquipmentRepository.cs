﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Application;
using DbItem = Askus.Store.DataBase.Proxy.Equipment;
using DomItem = Askus.Store.Infrastructure.Domain.Refbooks.Equipment;

namespace Askus.Store.Implementation.Repositories.Refbooks
{
    public class EquipmentRepository : DomainRepository<DomItem, DbItem>
    {
        public EquipmentRepository(DbEntityMediator dbContext, IUserContext context) : base(dbContext, context)
        {
        }
    }
}
