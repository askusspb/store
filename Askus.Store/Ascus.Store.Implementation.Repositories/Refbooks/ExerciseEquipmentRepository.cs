﻿using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Application;
using DbItem = Askus.Store.DataBase.Proxy.ExerciseEquipment;
using DomItem = Askus.Store.Infrastructure.Domain.Refbooks.ExerciseEquipment;

namespace Askus.Store.Implementation.Repositories.Refbooks
{
    public class ExerciseEquipmentRepository : DomainRepository<DomItem, DbItem>
    {
        public ExerciseEquipmentRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext, userContext)
        {
        }
    }
}
