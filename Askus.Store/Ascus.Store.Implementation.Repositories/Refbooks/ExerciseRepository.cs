﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Application;
using DbItem = Askus.Store.DataBase.Proxy.Exercise;
using DomItem = Askus.Store.Infrastructure.Domain.Refbooks.Exercise;

namespace Askus.Store.Implementation.Repositories.Refbooks
{
    public class ExerciseRepository : DomainRepository<DomItem, DbItem>
    {
        public ExerciseRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext, userContext)
        {
        }
    }
}
