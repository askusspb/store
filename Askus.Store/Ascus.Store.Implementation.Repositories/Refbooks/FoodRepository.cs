﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Application;
using DbItem = Askus.Store.DataBase.Proxy.Food;
using DomItem = Askus.Store.Infrastructure.Domain.Refbooks.Food;

namespace Askus.Store.Implementation.Repositories.Refbooks
{
    public class FoodRepository : DomainRepository<DomItem, DbItem>
    {
        public FoodRepository(DbEntityMediator dbContext, IUserContext userContext) : base(dbContext, userContext)
        {
        }
    }
}
