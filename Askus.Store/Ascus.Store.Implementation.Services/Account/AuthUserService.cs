﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Services.Account
{
    public class AuthUserService : BaseService, IAuthUserService
    {
        private readonly ICryptographyService _cryptographyService;
        private readonly IUserRepository _userRepository;

        public AuthUserService(ICryptographyService cryptographyService, IUserRepository userRepository)
        {
            _cryptographyService = cryptographyService;
            _userRepository = userRepository;
        }

        public async Task<User> ValidateUserAsync(string login, string password)
        {
            var user = await _userRepository.GetUserByLoginAsync(login);
            if (user == null)
                throw new InnerStoreException("Не корректные логин и/или пароль");

            if (!_cryptographyService.IsUserHashValid(user.PasswordHash, password, user.PasswordSalt))
                throw new InnerStoreException("Не корректные логин и/или пароль");

            user.LastLogin = DateTime.Now;
            await _userRepository.SaveAsync(user);

            return user;
        }
    }
}
