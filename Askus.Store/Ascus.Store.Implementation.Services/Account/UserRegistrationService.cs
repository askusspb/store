﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel.Account;
using System;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Services.Account
{
    public class UserRegistrationService : IUserRegistrationService
    {
        private readonly IUserRepository _userRepository;
        private readonly IDomainRepository<Profile> _profileRepository;
        private readonly ITransactionService _transactionService;
        private readonly ICryptographyService _cryptographyService;

        public UserRegistrationService(
            IUserRepository usrRep,
            IDomainRepository<Profile> prfRep,
            ITransactionService trnSrv,
            ICryptographyService crypto)
        {
            _userRepository = usrRep;
            _profileRepository = prfRep;
            _transactionService = trnSrv;
            _cryptographyService = crypto;
        }

        public async Task<User> RegisterUser(UserRegistrationVm regModel)
        {
            if (string.IsNullOrEmpty(regModel.Login))
                throw new ValidationException("The Login is required");
            if (string.IsNullOrEmpty(regModel.Password))
                throw new ValidationException("The Password is required");
            if (await _userRepository.GetUserByLoginAsync(regModel.Login) != null)
                throw new ValidationException("The user with save login already exist");

            User usr = new User()
            {
                Login = regModel.Login,
                LastLogin = DateTimeOffset.Now,
            };

            usr.PasswordSalt = usr.Login;
            usr.PasswordHash = _cryptographyService.GetUserPassHash(regModel.Password, usr.PasswordSalt);

            _transactionService.Begin();
            usr = await _userRepository.SaveAsync(usr);
            if (regModel.Profile != null)
            {
                regModel.Profile.UserId = usr.Id;
                regModel.Profile = await _profileRepository.SaveAsync(regModel.Profile);
            }
            _transactionService.Commit();
            return usr;
        }
    }
}
