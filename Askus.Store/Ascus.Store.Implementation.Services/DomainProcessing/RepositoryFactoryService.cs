﻿using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace Askus.Store.Implementation.Services.DomainProcessing
{
    public class RepositoryFactoryService : BaseService, IRepositoryFactoryService
    {
        private readonly IUnityContainer _container;
        private readonly IDictionary<Type, IStorageRepository> _repositoryDictionary = new Dictionary<Type, IStorageRepository>(); 

        public RepositoryFactoryService(IUnityContainer unity)
        {
            _container = unity;
        }

        public IStorageRepository GetRepository<TDomain>() where TDomain : BaseDomain
        {
            var targetDomainType = typeof(TDomain);
            return GetRepository(targetDomainType);
        }

        public IStorageRepository GetRepository(Type targetDomainType)
        {
            if (_repositoryDictionary.TryGetValue(targetDomainType, out IStorageRepository repositoryExist))
            {
                return repositoryExist;
            }

            var createdRepository = _container.Resolve(typeof(IDomainRepository<>).MakeGenericType(targetDomainType)) as IStorageRepository;
            _repositoryDictionary.Add(targetDomainType, createdRepository);
            return createdRepository;
        }
    }
}
