﻿
using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Askus.Store.Implementation.Services.DomainProcessing
{
    public class UnitOfWorkService : BaseService, IUnitOfWorkService
    {
        private readonly IRepositoryFactoryService _repositoryFactory;
        private readonly ILogger _logger;
        private readonly ITransactionService _transactionService;
        private readonly List<BaseDomain> _createdObjects = new List<BaseDomain>();
        private readonly List<BaseDomain> _updatedObjects = new List<BaseDomain>();
        private readonly List<BaseDomain> _deletedObjects = new List<BaseDomain>();

        public UnitOfWorkService(IRepositoryFactoryService repositoryFactory, ITransactionService transaction, ILogger logger)
        {
            _repositoryFactory = repositoryFactory;
            _transactionService = transaction;
            _logger = logger;
        }

        public async Task Commit()
        {
            try
            {
                _transactionService.Begin();
                var _createdTask = _createdObjects.Select(a => _repositoryFactory.GetRepository(a.GetType()).SaveAsync(a));
                var _updatedTask = _updatedObjects.Select(a => _repositoryFactory.GetRepository(a.GetType()).SaveAsync(a));
                var _removedTask = _deletedObjects.Select(a => _repositoryFactory.GetRepository(a.GetType()).RemoveAsync(a));
                await Task.WhenAll(_createdTask.Union(_updatedTask).Union(_removedTask));
                _transactionService.Commit();
            }
            catch (Exception ex)
            {
                _logger.LogException(ex);
                throw;
            }
        }

        public IUnitOfWorkService Delete(BaseDomain item)
        {
            _updatedObjects.Remove(item);
            _deletedObjects.Add(item);
            return this;
        }

        public IUnitOfWorkService New(BaseDomain item)
        {
            if (item.Id != 0)
                throw new InnerStoreException("Impossible to save as new exist item. Use method update");
            _createdObjects.Add(item);
            return this;
        }

        public IUnitOfWorkService Update(BaseDomain item)
        {
            if (item.Id == 0)
                throw new InnerStoreException("Impossible to update new item. Use method new");

            var existItem = _updatedObjects.SingleOrDefault(a => a.GetType() == item.GetType() && a.Id == item.Id);
            if (existItem != null)
            {
                _updatedObjects.Remove(existItem);
            }
            _updatedObjects.Add(item);
            return this;
        }
    }
}
