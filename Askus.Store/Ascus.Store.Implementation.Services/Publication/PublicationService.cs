﻿using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Services.Publication
{
    public class PublicationService : BaseService, IPublicationService
    {
        private readonly ILogger _logger;
        private readonly IUserContext _userContext;
        private readonly IDomainRepository<Page> _pageRepository;

        public PublicationService(ILogger logger, IUserContext userContext, IDomainRepository<Page> pageRepository)
        {
            _logger = logger;
            _userContext = userContext;
            _pageRepository = pageRepository;
        }

        public async Task SetPublication(Page page)
        {
            var user = _userContext.CurrentUser;
            //TODO: check permission

            var existPage = await _pageRepository.GetByIdAsync(page.Id);
            existPage.IsPublished = !existPage.IsPublished;
            await _pageRepository.SaveAsync(existPage);

            _logger.LogInfo($"Page {existPage.Id}/{existPage.Title} was published by user {user.Login}");
        }
    }
}
