﻿using Askus.Store.DataBase.Proxy.DbContextManagement;
using Askus.Store.Infrastructure.Interface.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Services
{
    public class TransactionService : BaseService, ITransactionService
    {
        private readonly DbTransactionMediator _dbTransactionMediator;
        public TransactionService(DbTransactionMediator dbTransactionMediator)
        {
            _dbTransactionMediator = dbTransactionMediator;
        }

        public void Begin()
        {
            _dbTransactionMediator.OpenTransaction();
        }

        public void Commit()
        {
            _dbTransactionMediator.CommitTranaction();
        }

        public void Rollback()
        {
            _dbTransactionMediator.Rollback();
        }
    }
}
