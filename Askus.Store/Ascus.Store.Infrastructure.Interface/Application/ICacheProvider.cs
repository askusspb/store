﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Application
{
    /// <summary>
    /// Оболочка для кеша
    /// </summary>
    public interface ICacheProvider
    {
        /// <summary>
        /// Возвращает данные из кеша по ключу. Если данных нет - вызывается getValue и добавляется в кеш
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getValue"></param>
        /// <returns></returns>
        T GetOrAdd<T>(string key, Func<T> getValue);

        /// <summary>
        /// Возвращает данные из кеша по ключу. Если данных нет - вызывается getValue и добавляется в кеш
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="getValue"></param>
        /// <returns></returns>
        Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getValue);
    }
}
