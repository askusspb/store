﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Application
{
    public interface ILogger
    {
        void LogException(Exception ex, string msg);

        void LogException(Exception ex);

        void LogInfo(string msg);
    }
}
