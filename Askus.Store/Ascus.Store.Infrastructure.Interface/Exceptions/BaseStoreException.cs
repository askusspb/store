﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Exceptions
{
    public abstract class BaseStoreException : Exception
    {
        public BaseStoreException(string message) : base(message) { }
    }
}
