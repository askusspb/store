﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Exceptions
{
    public class InnerStoreException : BaseStoreException
    {
        public InnerStoreException(string message) : base(message) { }
    }
}
