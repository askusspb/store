﻿using Askus.Store.Infrastructure.Interface.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Exceptions
{
    public class ValidationException : BaseStoreException
    {
        private readonly EValidationExceptionType _exceptionType;

        public ValidationException(string message) : base(message)
        {
            _exceptionType = EValidationExceptionType.Common;
        }

        public ValidationException(string message, EValidationExceptionType type) : base(message)
        {
            _exceptionType = type;
        }
    }
}
