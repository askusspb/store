﻿using Askus.Store.Infrastructure.Domain.Publication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
    public interface IArticleRepository : IDomainRepository<Article>
    {
        Task<Article> GetArticleByAliasAsyc(string alias);
    }
}
