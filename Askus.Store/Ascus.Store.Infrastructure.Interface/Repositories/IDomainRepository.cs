﻿using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
    public interface IDomainRepository<TDomain> : IBaseRepository
        where TDomain: BaseDomain
    {
        Task<IList<TDomain>> GetAllAsync();

        Task<GridVM<TDomain, TFilter>> GetFilteredAsync<TFilter>(TFilter filter)
            where TFilter : FilterVM;

        Task<TDomain> GetByIdAsync(int id);

        Task<TDomain> SaveAsync(TDomain model);

        Task RemoveAsync(TDomain model);
    }
}
