﻿using Askus.Store.Infrastructure.Domain.Publication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
    /// <summary>
    /// Репозиторий страниц портала
    /// </summary>
    public interface IPageRepository : IDomainRepository<Page>
    {
        /// <summary>
        /// Получить все опубликованные страницы
        /// </summary>
        /// <returns></returns>
        IList<Page> GetPublished();
    }
}
