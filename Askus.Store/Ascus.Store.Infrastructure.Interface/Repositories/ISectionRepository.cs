﻿using Askus.Store.Infrastructure.Domain.Publication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
    /// <summary>
    /// Репозиторий для работы с разделами сайта
    /// </summary>
    public interface ISectionRepository : IDomainRepository<Section>
    {
        IList<Section> GetPublished();
    }
}
