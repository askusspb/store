﻿using Askus.Store.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
   public  interface IStorageRepository : IBaseRepository
   {
        Task SaveAsync(BaseDomain item);

        Task RemoveAsync(BaseDomain item);
   }
}
