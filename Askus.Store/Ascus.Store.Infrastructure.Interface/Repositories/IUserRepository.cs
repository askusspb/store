﻿using Askus.Store.Infrastructure.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Repositories
{
    public interface IUserRepository : IDomainRepository<User>
    {
        Task<User> GetUserByLoginAsync(string login);
    }
}
