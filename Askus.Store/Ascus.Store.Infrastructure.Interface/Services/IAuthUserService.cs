﻿using Askus.Store.Infrastructure.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface IAuthUserService : IBaseService
    {
        Task<User> ValidateUserAsync(string login, string password);
    }
}
