﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface ICryptographyService : IBaseService
    {
        string Encrypt(string src);
        string Decrypt(string src);
        string GetUserPassHash(string pass, string salt);
        bool IsUserHashValid(string hash, string pass, string salt);
    }
}
