﻿using Askus.Store.Infrastructure.Domain.Publication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface IPublicationService : IBaseService
    {
        Task SetPublication(Page page);
    }
}
