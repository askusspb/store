﻿using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.Interface.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface IRepositoryFactoryService : IBaseService
    {
        IStorageRepository GetRepository<TDomain>()
            where TDomain : BaseDomain;

        IStorageRepository GetRepository(Type targetDomainType);
    }
}
