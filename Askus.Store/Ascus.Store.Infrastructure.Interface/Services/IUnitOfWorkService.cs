﻿using Askus.Store.Infrastructure.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface IUnitOfWorkService : IBaseService
    {
        IUnitOfWorkService New(BaseDomain item);

        IUnitOfWorkService Update(BaseDomain item);

        IUnitOfWorkService Delete(BaseDomain item);

        Task Commit();
    }
}
