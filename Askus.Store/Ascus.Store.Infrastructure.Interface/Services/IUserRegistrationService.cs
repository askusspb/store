﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Interface.Services
{
    public interface IUserRegistrationService : IBaseService
    {
        Task<User> RegisterUser(UserRegistrationVm regModel);
    }
}
