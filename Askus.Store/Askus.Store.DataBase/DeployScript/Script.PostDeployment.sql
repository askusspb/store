﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Default user
 IF NOT EXISTS (SELECT [Login] FROM [Account].[User] WHERE [Login]='Admin')
   BEGIN
      INSERT INTO [Account].[User] ([Login], [PasswordHash], [PasswordSalt])	
      VALUES ('Admin', 'BOHrVNMnCvbUxiJ92Qo3Rv8UmtEoYpU6jSXKzVqMhE0jptrjW45JCOBqZqib5s4PYG3IS+/wTMsEWBYxA4VRmS63xnX7reyNkrZswG2E6vlgv/aj1Y1Q6uGkSrQMFDAe', '') 
   END

