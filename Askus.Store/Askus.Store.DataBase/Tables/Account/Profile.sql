﻿CREATE TABLE [Account].[Profile]
(
	[Id] INT NOT NULL PRIMARY KEY, 
	[UserId] INT NOT NULL ,
    [FullName] NVARCHAR(500) NOT NULL, 
    CONSTRAINT [FK_Profile_ToUser] FOREIGN KEY ([UserId]) REFERENCES [Account].[User]([Id])
    
)
