﻿CREATE TABLE [Account].[User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Login] NVARCHAR(100) NOT NULL, 
    [PasswordHash] NVARCHAR(250) NOT NULL, 
    [PasswordSalt] NVARCHAR(50) NOT NULL, 
    [DisabledTo] DATETIMEOFFSET NULL , 
    [LastLogin] DATETIMEOFFSET NULL, 
    [FailAttempt] NCHAR(10) NULL
)
