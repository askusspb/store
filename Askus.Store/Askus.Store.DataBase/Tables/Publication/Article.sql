﻿CREATE TABLE [Publication].[Article]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
	[Title] NVARCHAR(250) NULL, 
    [Annotation] NVARCHAR(500) NULL, 
	[Content] NVARCHAR(MAX) NULL, 
    [Alias] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_Article_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Article_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id])
)
