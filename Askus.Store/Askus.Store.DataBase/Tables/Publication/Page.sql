﻿CREATE TABLE [Publication].[Page]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
	[Title] NVARCHAR(250) NULL, 
    [SectionId] INT NULL, 
	[ControllerName] NVARCHAR(100) NOT NULL,
	[ActionName] NVARCHAR(100) NOT NULL,
	[Key] NVARCHAR(100) NOT NULL,
    [IsPublished] BIT NOT NULL DEFAULT 0, 
    CONSTRAINT [FK_Page_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Page_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id])
)
