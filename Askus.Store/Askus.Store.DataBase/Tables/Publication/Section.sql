﻿CREATE TABLE [Publication].[Section]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
	[ParentSectionId] INT NULL, 
    [Title] NVARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_Section_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Section_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id])
)
