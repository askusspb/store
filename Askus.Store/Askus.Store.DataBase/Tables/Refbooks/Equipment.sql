﻿CREATE TABLE [Refbooks].[Equipment]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Aka] NVARCHAR(100) NOT NULL, 
    [Title] NVARCHAR(250) NULL, 
    [Description] NVARCHAR(500) NULL, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
    CONSTRAINT [FK_Equipment_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Equipment_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id])
)
