﻿CREATE TABLE [Refbooks].[Exercise]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Aka] NVARCHAR(100) NOT NULL, 
    [Title] NVARCHAR(250) NULL, 
    [Description] NVARCHAR(500) NULL, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
    [RequiredEnergy] DECIMAL(18, 2) NOT NULL, 
    [ArticleId] INT NULL, 
    CONSTRAINT [FK_Exercise_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Exercise_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id]), 
    CONSTRAINT [FK_Exercise_ToArticle] FOREIGN KEY ([ArticleId]) REFERENCES [Publication].[Article]([Id])
)
