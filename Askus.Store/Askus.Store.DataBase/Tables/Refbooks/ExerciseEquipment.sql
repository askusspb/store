﻿CREATE TABLE [Refbooks].[ExerciseEquipment]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
    [ExerciseId] INT NOT NULL, 
    [EquipmentId] INT NOT NULL, 
    CONSTRAINT [FK_ExetcideEquipment_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_ExetcideEquipment_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id]), 
    CONSTRAINT [FK_ExetcideEquipment_ToExercise] FOREIGN KEY ([ExerciseId]) REFERENCES [Refbooks].[Exercise]([Id]), 
	CONSTRAINT [FK_ExetcideEquipment_ToEquipment] FOREIGN KEY ([ExerciseId]) REFERENCES [Refbooks].[Equipment]([Id]), 
)
