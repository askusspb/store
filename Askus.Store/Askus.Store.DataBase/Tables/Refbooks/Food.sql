﻿CREATE TABLE [Refbooks].[Food]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Aka] NVARCHAR(100) NOT NULL, 
    [Title] NVARCHAR(250) NULL, 
    [Description] NVARCHAR(500) NULL, 
    [CreatedDate] DATETIMEOFFSET NOT NULL DEFAULT GETDATE(), 
    [LastUpdatedDateTime] DATETIMEOFFSET NULL, 
    [CreatedUserId] INT NOT NULL, 
    [UpdatedUserId] INT NULL, 
    [ArticleId] INT NULL, 
    [Calories] DECIMAL(18, 2) NOT NULL, 
    [Carbohydrates] DECIMAL(18, 2) NOT NULL, 
    [Proteins] DECIMAL(18, 2) NOT NULL, 
    [Fats] DECIMAL(18, 2) NOT NULL, 
    CONSTRAINT [FK_Food_ToUserCreated] FOREIGN KEY ([CreatedUserId]) REFERENCES [Account].[User]([Id]),
	CONSTRAINT [FK_Food_ToUserUpdated] FOREIGN KEY ([UpdatedUserId]) REFERENCES [Account].[User]([Id])
)
