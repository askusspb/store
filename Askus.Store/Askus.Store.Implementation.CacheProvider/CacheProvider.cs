﻿using Askus.Store.Infrastructure.Interface.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.CacheProvider
{
    public class CacheProvider : ICacheProvider
    {
        public T GetOrAdd<T>(string key, Func<T> getValue)
        {
            throw new NotImplementedException();
        }

        public Task<T> GetOrAddAsync<T>(string key, Func<Task<T>> getValue)
        {
            throw new NotImplementedException();
        }
    }
}
