﻿using Askus.Store.Infrastructure.Interface.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.CacheProvider
{
    public class EmptyCacheProvider : ICacheProvider
    {
        T ICacheProvider.GetOrAdd<T>(string key, Func<T> getValue)
        {
            return getValue();
        }

        Task<T> ICacheProvider.GetOrAddAsync<T>(string key, Func<Task<T>> getValue)
        {
            return getValue();
        }
    }
}
