﻿using Askus.Store.Infrastructure.Interface.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Logger
{
    public class EmptyLogWrapper : ILogger
    {
        public void LogException(Exception ex, string msg)
        {
            //Do nothing
        }

        public void LogException(Exception ex)
        {
           //Do nothing
        }

        public void LogInfo(string msg)
        {
            //Do nothing
        }
    }
}
