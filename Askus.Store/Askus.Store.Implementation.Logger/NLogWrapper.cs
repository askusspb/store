﻿
using Askus.Store.Infrastructure.Interface.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Implementation.Logger
{
    public sealed class NLogWrapper : ILogger
    {
        private readonly NLog.Logger _logger;

        public NLogWrapper()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void LogException(Exception ex, string msg)
        {
            _logger.Log(NLog.LogLevel.Error, ex, msg);
        }

        public void LogException(Exception ex)
        {
            _logger.Log(NLog.LogLevel.Error, ex);
        }

        public void LogInfo(string msg)
        {
            _logger.Log(NLog.LogLevel.Info, msg);
        }
    }
}
