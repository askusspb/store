﻿using Askus.Store.Infrastructure.Domain.Refbooks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Exercise.Refbooks
{
    public class Activity : BaseRefbook
    {
        public int? ArticleId { get; set; }

        public decimal RequiredEnergy { get; set; }
    }
}
