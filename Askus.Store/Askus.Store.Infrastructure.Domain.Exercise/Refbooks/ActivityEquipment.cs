﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Exercise.Refbooks
{
    public class ActivityEquipment : BaseDomain
    {
        public int ActivityId { get; set; }

        public int EquipmentId { get; set; }
    }
}
