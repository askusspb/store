﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Account
{
    public class Profile : BaseDomain
    {
        public int UserId { get; set; }

        public string FullName { get; set; }
    }
}
