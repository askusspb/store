﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Account
{
    public class User : BaseDomain
    {
        public string Login { get; set; }

        public string PasswordHash { get; set; }
    
        public string PasswordSalt { get; set; }

        public DateTimeOffset? LastLogin { get; set; }

        public DateTimeOffset? DisabledTo { get; set; }
    }
}
