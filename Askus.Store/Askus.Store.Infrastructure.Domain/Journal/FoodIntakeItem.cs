﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Journal
{
    public class FoodIntakeItem : BaseDomain
    {
        public int FoodIntakeSessionId { get; set; }

        public int FoodId { get; set; }

        public decimal Weigth { get; set; }
    }
}
