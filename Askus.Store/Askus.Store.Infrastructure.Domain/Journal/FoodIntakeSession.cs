﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Journal
{
    public class FoodIntakeSession : BaseDomain
    {
        public string Title { get; set; }

        public DateTimeOffset DateTime { get; set; }
    }
}
