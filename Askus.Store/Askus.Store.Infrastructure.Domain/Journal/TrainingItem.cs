﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Journal
{
    public class TrainingItem : BaseDomain
    {
        public int TrainingSessionId { get; set; }

        public int ExerciseId { get; set; }

        public int Iteration { get; set; }

        public decimal EquipmentWeigth { get; set; }

        public TimeSpan TotalTime { get; set; }
    }
}
