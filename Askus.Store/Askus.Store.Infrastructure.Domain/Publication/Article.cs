﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Publication
{
    public class Article : BaseDomain
    {
        public DateTime CreatedDate { get; set; }

        public DateTime LastUpdatedDateTime { get; set; }

        public string Alias { get; set; }

        public string Title { get; set; }

        public string Annotation { get; set; }

        public string Content { get; set; }
    }
}
