﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Publication
{
    public class Page : BaseDomain
    {
        public int SectionId { get; set; }

        public string Title { get; set; }

        public string ActionName { get; set; }

        public string ControllerName { get; set; }

        public string Key { get; set; }

        public bool IsPublished { get; set; }
    }
}
