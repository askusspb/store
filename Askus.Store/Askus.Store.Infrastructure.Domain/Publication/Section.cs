﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Publication
{
    public class Section : BaseDomain
    {
        public int? ParentSectionId { get; set; }

        public string Title { get; set; }
    }
}
