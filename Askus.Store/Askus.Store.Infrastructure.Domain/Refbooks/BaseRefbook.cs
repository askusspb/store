﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Refbooks
{
    public class BaseRefbook : BaseDomain
    {
        public string Aka { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
