﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Refbooks
{
    public class ExerciseEquipment : BaseDomain
    {
        public int ExerciseId { get; set; }

        public int EquipmentId { get; set; }
    }
}
