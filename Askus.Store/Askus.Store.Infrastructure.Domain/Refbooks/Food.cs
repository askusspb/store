﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.Domain.Refbooks
{
    public class Food : BaseRefbook
    {
        public decimal Calories { get; set; }

        public decimal Carbohydrates { get; set; }

        public decimal Proteins { get; set; }

        public decimal Fats { get; set; }
    }
}
