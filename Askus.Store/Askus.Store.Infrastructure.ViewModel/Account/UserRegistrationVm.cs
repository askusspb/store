﻿using Askus.Store.Infrastructure.Domain.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.ViewModel.Account
{
    public class UserRegistrationVm : BaseVM
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public Profile Profile { get; set; }
    }
}
