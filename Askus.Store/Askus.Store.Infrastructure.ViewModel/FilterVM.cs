﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.ViewModel
{
    public class FilterVM : BaseVM
    {
        public int? Id { get; set; }

        public int? Skip { get; set; }

        public int? Take { get; set; }

        public string Query { get; set; }
    }
}
