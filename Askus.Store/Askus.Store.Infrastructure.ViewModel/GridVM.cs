﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.ViewModel
{
    public class GridVM<TItem, TFilter> : BaseVM
        where TItem: class
    {
        public IEnumerable<TItem> Items { get; set; }

        public TFilter Filter { get; set; }

        public int Total { get; set; }
    }
}
