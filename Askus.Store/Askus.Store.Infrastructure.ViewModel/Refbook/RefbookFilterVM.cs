﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Askus.Store.Infrastructure.ViewModel.Refbook
{
    public class RefbookFilterVM : FilterVM
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }
}
