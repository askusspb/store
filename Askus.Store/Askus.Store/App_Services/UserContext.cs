﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Interface.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Askus.Store
{
    public class UserContext : IUserContext
    {
        public User CurrentUser { get; set; }

        public UserContext()
        {
            CurrentUser = HttpContext.Current.Session["CurrentUser"] as User;
        }
    }
}