﻿
using System.Web;
using System.Web.Optimization;

namespace Askus.Store
{
    public static class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.CreateScriptBnd(BundleScriptNames.BaseLibrary)
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery.validate*")
                .Include("~/Scripts/modernizr-*")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/respond.js")
                .Include("~/ClientApp/Pages/login.js");

            bundles.CreateScriptBnd(BundleScriptNames.Knockout)
                .Include("~/bower_components/knockout/dist/knockout.js")
                .Include("~/bower_components/knockout/dist/knockout.mapping.js");

            bundles.CreateScriptBnd(BundleScriptNames.SectionPage)
                .Include("~/ClientApp/Pages/utils.js")
                .Include("~/ClientApp/Pages/sectionsPage.js");

            bundles.CreateScriptBnd(BundleScriptNames.PagesPage)
                .Include("~/ClientApp/Pages/utils.js")
                .Include("~/ClientApp/Pages/pagesPage.js");

            bundles.CreateScriptBnd(BundleScriptNames.ArticlePage)
                .Include("~/ClientApp/Pages/utils.js")
                .Include("~/ClientApp/Pages/articlePage.js");

            bundles.CreateStyleBnd(BundleStyleNames.BaseCss)
                .Include( "~/Content/bootstrap.css")
                .Include("~/Content/site.css");
        }

        #region private ext

        private static ScriptBundle CreateScriptBnd(this BundleCollection bundles, string name)
        {
            var bndl = new ScriptBundle(name);
            bundles.Add(bndl);
            return bndl;
        }

        private static StyleBundle CreateStyleBnd(this BundleCollection bundles, string name)
        {
            var bndl = new StyleBundle(name);
            bundles.Add(bndl);
            return bndl;
        }
        
        #endregion
    }
}
