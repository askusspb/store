﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Askus.Store
{
    public static class BundleScriptNames
    {
        public const string BaseLibrary = "~/bundles/library";

        public const string Knockout = "~/bundles/knockout";

        #region Pages

        public const string SectionPage = "~/bundles/section";

        public const string PagesPage = "~/bundles/pages";

        public const string ArticlePage = "~/bundles/article";

        #endregion

    }
}