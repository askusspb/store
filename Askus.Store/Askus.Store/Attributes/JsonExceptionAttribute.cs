﻿using Askus.Store.Models.ExceptionProcessing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Askus.Store.Attributes
{
    public class JsonExceptionAttribute : ActionFilterAttribute, IExceptionFilter
    {

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.ContentType = "application/json";
            filterContext.HttpContext.Response.Write(
                Newtonsoft.Json.JsonConvert.SerializeObject(
                    new CommonExeptionModel() { Message = filterContext.Exception.Message }));
            filterContext.ExceptionHandled = true;
        }
    }
}