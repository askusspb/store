﻿var Askus = Askus || {};
Askus.ArticlePage = Askus.ArticlePage || {};

Askus.ArticlePage.Article = function () {
    var _self = this;
    _self.Id = ko.observable();
    _self.CreateDate = ko.observable();
    _self.LastUpdatedDateTime = ko.observable();
    _self.Title = ko.observable();
    _self.Annotation = ko.observable();
    _self.Content = ko.observable();
    _self.Alias = ko.observable();
}

//--------- Filter

Askus.ArticlePage.Filter = function (p) {
    var _self = this;
    _self.PageCapacity = p.PageCapacity;
    _self.Total = ko.observable(p.PageCapacity);
    _self.Page = ko.observable(0);

    _self.Take = ko.computed(function () {
        return _self.PageCapacity;
    });
    _self.Skip = ko.computed(function () {
        return _self.Page() * _self.PageCapacity;
    });

    _self.GetFilter = function () {
        return {
            Skip: _self.Page() * _self.PageCapacity,
            Take: _self.PageCapacity
        }
    }
    _self.SetPage = function (p) {
        _self.Page(p);
    }

    _self.GetTotalPages = function () {
        return Math.floor(_self.Total() / _self.PageCapacity) + (_self.Total() % _self.PageCapacity > 0 ? 1 : 0);
    }

    _self.AvailablePages = ko.computed(function () {
        var pages = new Array();
        if (_self.Page() > 0)
            pages.push(0);
        if (_self.Page() > 1)
            pages.push(_self.Page() - 1);
        pages.push(_self.Page());
        if (_self.Page() < _self.GetTotalPages() - 1)
            pages.push(_self.Page() + 1);
        if (_self.Page() + 1 < _self.GetTotalPages() - 1)
            pages.push(_self.GetTotalPages());
        return pages;
    });
}

//--------- Application

Askus.ArticlePage.App = function (p) {
    var _self = this;

    _self.Params = p;
    _self.Items = ko.observableArray(new Array());
    _self.Filter = new Askus.ArticlePage.Filter(p);
    _self.CurrentItem = new Askus.ArticlePage.Article();
    _self.ContentVersion = ko.observable(1);
    _self.ShowEditorLink = true;

    _self.FillCollection = function (src) {
        _self.Items(ko.mapping.fromJS(src.Items)());
        _self.Filter.Total(src.Total);
    }

    _self.UpdateContent = ko.computed(function () {
        _self.ContentVersion();
        Utils.GetAjax(
            _self.Params.GetUrl,
            _self.Filter.GetFilter(),
            _self.FillCollection);
    })

    _self.Edit = function (item) {
        _self.CurrentItem = item;
        Utils.ShowModalForm({
            title: "Редактор статьи",
            editorSelector: p.EditorSelector,
            url: _self.Params.SaveUrl,
            getFormData: function () { return ko.mapping.toJS(_self.CurrentItem); },
            callback: function (r) { _self.ContentVersion(_self.ContentVersion() + 1); },
        });

        ko.cleanNode($("#modal_form").find(".modal-body")[0])
        ko.applyBindings(_self, $("#modal_form").find(".modal-body")[0]);
    }

    _self.Create = function () {
        var item = new Askus.ArticlePage.Article();
        _self.Edit(item);
    }

    _self.GetItemTitle = function (id) {
        var item = _self.Items().find(function (el, ind, arr) { return el.Id() == id });
        return item ? item.Title() : "";
    }

    _self.EditorUrl = function () {
        return _self.Params.EditorUrl + "?id=" + _self.CurrentItem.Id(); 
    }

    _self.Remove = function (item) {

        Utils.ShowModalForm({
            title: "Удаление статьи",
            editorSelector: "<p> Удалить статью " + item.Title() + " </p>",
            url: _self.Params.RemoveUrl,
            getFormData: function () {
                return { id: item.Id() };
            },
            callback: function (r) { _self.ContentVersion(_self.ContentVersion() + 1); },
        });
    }

    ko.applyBindings(_self, document.getElementById(p.AppSelector));
}

Askus.ArticlePage.EditorApp = function (p) {
    _self = this;
    _self.Params = p;
    _self.CurrentItem = new Askus.ArticlePage.Article();

    _self.EditorUrl = function () {
        return _self.Params.EditorUrl + "?id=" + _self.CurrentItem.Id();
    }

    if (p.Item) {
        _self.CurrentItem = ko.mapping.fromJSON(p.Item);
    }

    ko.applyBindings(_self, document.getElementById(p.AppSelector));
}