﻿$(function () {
    $("#modal_auth_login").click(function () {
        var r = {};
        r.url = $(this).attr("data-url");
        r.type = "POST";
        r.data = {
            login: $("#auth_login").val(),
            password: $("#auth_pass").val(),
        };
        $.ajax(r)
            .done(function (rs) {
                location.reload();
            })
            .fail(function (rs) {
                $("#auth_errortext").text(rs.responseJSON.Message);
                $("#auth_errortext").closest(".row").removeClass("hidden");
            });
    });
})