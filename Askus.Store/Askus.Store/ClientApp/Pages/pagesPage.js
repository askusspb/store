﻿var Askus = Askus || {};
Askus.PagesPage = Askus.PagesPage || {};

//-------- 

Askus.PagesPage.Page = function () {
    var _self = this;
    _self.Id = ko.observable();
    _self.Title = ko.observable();
    _self.ParentSectionId = ko.observable();
    _self.Title = ko.observable();
    _self.ControllerName = ko.observable();
    _self.ActionName = ko.observable();
    _self.Key = ko.observable();
    _self.IsPublished = ko.observable();
}

Askus.PagesPage.Filter = function (p) {
    var _self = this;
    _self.PageCapacity = p.PageCapacity;
    _self.Total = ko.observable(p.PageCapacity);

    _self.Page = ko.observable(0);
    _self.Take = ko.computed(function () {
        return _self.PageCapacity;
    });
    _self.Skip = ko.computed(function () {
        return _self.Page() * _self.PageCapacity;
    });

    _self.GetFilter = function () {
        return {
            Skip: _self.Page() * _self.PageCapacity,
            Take: _self.PageCapacity
        }
    }
    _self.SetPage = function (p) {
        _self.Page(p);
    }

    _self.GetTotalPages = function () {
        return Math.floor(_self.Total() / _self.PageCapacity) + (_self.Total() % _self.PageCapacity > 0 ? 1 : 0);
    }

    _self.AvailablePages = ko.computed(function () {
        var pages = new Array();
        if (_self.Page() > 0)
            pages.push(0);
        if (_self.Page() > 1)
            pages.push(_self.Page() - 1);
        pages.push(_self.Page());
        if (_self.Page() < _self.GetTotalPages() - 1)
            pages.push(_self.Page() + 1);
        if (_self.Page() + 1 < _self.GetTotalPages() - 1)
            pages.push(_self.GetTotalPages());
        return pages;
    });
}

Askus.PagesPage.App = function (p) {
    var _self = this;

    _self.Params = p;
    _self.Items = ko.observableArray(new Array());
    _self.Sections = ko.observableArray(new Array());
    _self.Filter = new Askus.PagesPage.Filter(p);
    _self.CurrentItem = new Askus.PagesPage.Page();
    _self.ContentVersion = ko.observable(1);

    _self.FillCollection = function (src) {
        _self.Items(ko.mapping.fromJS(src.Items)());
        _self.Filter.Total(src.Total);
    }

    _self.UpdateContent = ko.computed(function () {
        _self.ContentVersion();
        Utils.GetAjax(
            _self.Params.GetUrl,
            _self.Filter.GetFilter(),
            _self.FillCollection);
    })

    _self.Edit = function (item) {
        _self.CurrentItem = item;
        Utils.ShowModalForm({
            title: "Редактор страницы",
            editorSelector: p.EditorSelector,
            url: _self.Params.SaveUrl,
            getFormData: function () { return ko.mapping.toJS(_self.CurrentItem); },
            callback: function (r) { _self.ContentVersion(_self.ContentVersion() + 1); },
        });

        ko.cleanNode($("#modal_form").find(".modal-body")[0])
        ko.applyBindings(_self, $("#modal_form").find(".modal-body")[0]);
    }

    _self.Create = function () {
        var item = new Askus.PagesPage.Page();
        _self.Edit(item);
    }

    _self.GetSectionTitle = function (id) {
        var item = _self.Sections().find(function (el, ind, arr) { return el.Id() == id });
        return item ? item.Title() : "";
    }

    _self.SetPublication = function (item) {

        var actionName = item.IsPublished() ? "Снять с публикации" : "Опубликовать";

        Utils.ShowModalForm({
            title: "Публикация страницы",
            editorSelector: "<p>" + actionName + " страницу " + item.Title() + "</p>",
            url: _self.Params.SetPublicationUrl,
            getFormData: function () {
                return { id: item.Id() };
            },
            callback: function (r) { _self.ContentVersion(_self.ContentVersion() + 1); },
        });
    }

    _self.Remove = function (item) {

        Utils.ShowModalForm({
            title: "Удаление страницы",
            editorSelector: "<p> Удалить страницу " + item.Title() + " </p>",
            url: _self.Params.RemoveUrl,
            getFormData: function () {
                return { id: item.Id() };
            },
            callback: function (r) { _self.ContentVersion(_self.ContentVersion() + 1); },
        });
    }


    Utils.GetAjax(
        _self.Params.SectionsUrl,
        {},
        function (src) { _self.Sections(ko.mapping.fromJS(src.Items)()); }
    );
    ko.applyBindings(_self, document.getElementById(p.AppSelector));
}
