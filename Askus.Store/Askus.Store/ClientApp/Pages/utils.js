﻿var Utils = Utils || {};

Utils.ShowModalInfo = function (title, content) {
    $("#modal_info").find(".modal-title").html(title);
    $("#modal_info").find(".modal-body").html(content);
    $("#modal_info").modal();
}

Utils.ShowModalForm = function (p) {

    if (!p.title) console.error("ShowModalForm: title is not defined");
    if (!p.editorSelector) console.error("ShowModalForm: editorSelector is not defined");
    if (!p.url) console.error("ShowModalForm: url is not defined");
    if (!p.getFormData) console.error("ShowModalForm: getFormData is not defined");
    if (!p.callback) console.error("ShowModalForm: callback is not defined");
    
    $("#modal_form").find(".alert").addClass("hidden");
    $("#modal_form").find(".modal-title").html(p.title);
    $("#modal_form").find(".modal-body").html($(p.editorSelector).html());
    $("#modal_form").modal();

    $("#modal_form").find(".btn:first").unbind().click(function () {
        var model = (p.getFormData ? p.getFormData() : {});
        Utils.PostAjax(
            p.url,
            model,
            function (r)
            {
                $("#modal_form").modal('toggle');
                if (p.callback)
                    p.callback(r);
            },
            function (r) {
                $("#modal_form")
                    .find(".alert")
                    .removeClass("hidden")
                    .children("span")
                    .text(r.responseJSON.Message);
            });
    });
}

Utils.GetAjax = function (url, params, callback)
{
    var request = {};
    request.url = url;
    request.type = "GET";

    if (params)
        request.data = params;

    $.ajax(request)
    .done(function (r) {
        if (callback) {
            callback(r);
        }
    })
    .fail(function (r) {
        var message = r.responseJSON.Message ? r.responseJSON.Message : "Произошла неизвестная ошибка";
        Utils.ShowModalInfo("Произошла ошибка", message);
    });
}

Utils.PostAjax = function (url, params,onDone, onFail) {
    var request = {};
    request.url = url;
    request.type = "POST";

    if (params)
        request.data = params;

    $.ajax(request)
        .done(function (r) {
            if (onDone) {
                onDone(r);
            }
        })
        .fail(function (r) {
            if (onFail)
                onFail(r)
            else {
                var message = r.responseJSON.Message ? r.responseJSON.Message : "Произошла неизвестная ошибка";
                Utils.ShowModal("Произошла ошибка", message);
            }
        });
}

Utils.FormatDate = function (d) {
    return d;
}