﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel;

namespace Askus.Store.Controllers
{
    public class ArticleController : BaseDomainController<Article, FilterVM>
    {
        public ArticleController(IDomainRepository<Article> repository) : base(repository)
        {
        }

        [HttpGet]
        public async Task<ViewResult> Edit(int id)
        {
            var item = await Repository.GetByIdAsync(id) ?? new Article();
            return View(item);
        }

        [HttpGet]
        public PartialViewResult RenderArticle(string alias)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public PartialViewResult RenderPreview(string alias)
        {
            throw new NotImplementedException();
        }


        protected override void SaveValidation(Article item)
        {
            if (item == null)
                throw new ValidationException("Данные не найдены");
            if (string.IsNullOrEmpty(item.Alias))
                throw new ValidationException("Необходимо указать Action");
            if (string.IsNullOrEmpty(item.Content))
                throw new ValidationException("Необходимо указать Controller");
        }
    }
}