﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Models.Page;

namespace Askus.Store.Controllers
{
    public class AuthController : BaseStoreController
    {
        private readonly IAuthUserService _authUserService;

        public AuthController(IAuthUserService authUserService)
        {
            _authUserService = authUserService;
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Login(LoginModel model)
        {
            var user = await _authUserService.ValidateUserAsync(model.Login, model.Password);
            Session["CurrentUser"] = user;
        }

        [AjaxOnly]
        [JsonException]
        public void Logout()
        {
            Session["CurrentUser"] = null;
        }

    }
}