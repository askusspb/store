﻿using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Domain;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.ViewModel;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    public abstract class BaseDomainController<TDomain, TFilter> : BaseStoreController
        where TDomain : BaseDomain
        where TFilter: FilterVM
    {
        protected IDomainRepository<TDomain> Repository { get; private set; }

        public BaseDomainController(IDomainRepository<TDomain> repository)
        {
            Repository = repository;
        }

        /// <summary>
        /// Start page
        /// </summary>
        /// <returns></returns>
        public virtual ViewResult Index()
        {
            return View();
        }

        [AjaxOnly]
        [JsonException]
        [HttpGet]
        public async virtual Task<JsonResult> GetList(TFilter filter)
        {
            var data = await Repository.GetFilteredAsync<TFilter>(filter);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Save(TDomain model)
        {
            SaveValidation(model);
            await Repository.SaveAsync(model);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Remove(int id)
        {
            var item = await Repository.GetByIdAsync(id);
            await Repository.RemoveAsync(item);
        }

        protected virtual void SaveValidation(TDomain item)
        {
        }

    }
}