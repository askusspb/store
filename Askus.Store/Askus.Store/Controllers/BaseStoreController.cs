﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Models.Page;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    public abstract class BaseStoreController : Controller
    {
        private const string MenuViewName = "_Menu";
        private const string TitleViewName = "_Title";
        private const string FooterViewName = "_Footer";
        private const string MenuCacheKey = "Cache/Menu";
        protected const int MaxPageCount = 20;

        private readonly IPageRepository _pageRepo;
        private readonly ISectionRepository _sectionRepo;
        private readonly ICacheProvider _cacheProvider;

        public BaseStoreController()
        {
            _pageRepo = DependencyResolver.Current.GetService<IDomainRepository<Page>>() as IPageRepository;
            _sectionRepo = DependencyResolver.Current.GetService<IDomainRepository<Section>>() as ISectionRepository;
            _cacheProvider = DependencyResolver.Current.GetService<ICacheProvider>();
        }

        [ChildActionOnly]
        public virtual PartialViewResult Menu()
        {
            var menu = new MenuModel();
            menu.IsAutorized = CurrentUser != null;
            menu.UserName = CurrentUser?.Login;
            menu.Items = GetMenuItems();

            return PartialView(MenuViewName, menu);
        }

        [ChildActionOnly]
        public virtual PartialViewResult Title()
        {
            return PartialView(TitleViewName);
        }

        [ChildActionOnly]
        public virtual PartialViewResult Footer()
        {
            return PartialView(FooterViewName);
        }

        private void ExtendAdminMenu(List<MenuItemModel> src)
        {
            var settingsNode = new MenuItemModel() { Title = "Настройки" };
            settingsNode.ChildItems = new List<MenuItemModel>();

            settingsNode.ChildItems.Add(new MenuItemModel() { Title = "Статьи", ControllerName = "Article" });
            settingsNode.ChildItems.Add(new MenuItemModel() { Title = "Разделы", ControllerName = "Section" });
            settingsNode.ChildItems.Add(new MenuItemModel() { Title = "Страницы", ControllerName = "Page" });

            src.Add(settingsNode);
        }

        protected List<MenuItemModel> GetMenuItems()
        {
            var items = _cacheProvider.GetOrAdd(MenuCacheKey, () => BuildMenu());

            if (CurrentUser?.Login == "Admin")
                ExtendAdminMenu(items);

            return items;
        }

        private List<MenuItemModel> BuildMenu()
        {
            var sections = _sectionRepo.GetPublished();
            var pages = _pageRepo.GetPublished();

            return sections
                .Where(a => a.ParentSectionId == null)
                .Select(a => BuildNode(a, sections, pages))
                .ToList();
        }

        private MenuItemModel BuildNode(Section section, IEnumerable<Section> sections, IEnumerable<Page> pages)
        {
            var res = new MenuItemModel() { SectionId = section.Id, Title = section.Title };
            res.ChildItems = new List<MenuItemModel>();
            res.ActionName = "Map";
            res.ControllerName = "Section";
            res.Params = new { Id = section.Id };
            foreach (var p in pages.Where(a => a.SectionId == section.Id))
            {
                var c = new MenuItemModel() { Title = p.Title };
                c.ActionName = p.ActionName;
                c.ControllerName = p.ControllerName;
                c.Params = new { Key = p.Key };
                res.ChildItems.Add(c);
            }

            foreach (var s in sections.Where(a => a.ParentSectionId == section.Id))
            {
                res.ChildItems.Add(BuildNode(s, sections, pages));
            }

            return res;
        }

        private User CurrentUser => Session["CurrentUser"] as User; //TODO: move to better place
    }
}