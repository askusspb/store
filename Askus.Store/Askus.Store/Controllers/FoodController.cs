﻿using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Domain.Refbooks;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    public class FoodController : BaseDomainController<Food, FilterVM>
    {
        public FoodController(IDomainRepository<Food> repository) : base(repository)
        {
        }
    }
}