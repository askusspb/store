﻿using Askus.Store.Infrastructure.Domain.Account;
using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    public class HomeController : BaseStoreController
    {
        public HomeController()
        {
            var logger = DependencyResolver.Current.GetService<ILogger>();
        }

        public ActionResult Index()
        { 
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}