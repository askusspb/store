﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel;

namespace Askus.Store.Controllers
{
    public class PageController : BaseStoreController
    {
        private IDomainRepository<Page> _pageRepository;
        private IPublicationService _publicationService;

        public PageController(
            IDomainRepository<Page> domainRepository,
            IPublicationService publicationService)
        {
            _pageRepository = domainRepository;
            _publicationService = publicationService;
        }

        // GET: Page
        public ViewResult Index()
        {
            return View();
        }

        [AjaxOnly]
        [JsonException]
        [HttpGet]
        public async Task<JsonResult> GetList(FilterVM filter)
        {
            if (filter == null)
                filter = new FilterVM() { Skip = 0, Take = MaxPageCount };

            var data = await _pageRepository.GetFilteredAsync(filter);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Save(Page item)
        {
            if (item == null)
                throw new ValidationException("Данные не найдены");
            if (string.IsNullOrEmpty(item.ActionName))
                throw new ValidationException("Необходимо указать Action");
            if (string.IsNullOrEmpty(item.ControllerName))
                throw new ValidationException("Необходимо указать Controller");

            await _pageRepository.SaveAsync(item);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Remove(int id)
        {
            var page = await _pageRepository.GetByIdAsync(id);
            await _pageRepository.RemoveAsync(page);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task SetPublication(int id)
        {
            var page = await _pageRepository.GetByIdAsync(id);
            await _publicationService.SetPublication(page);
        }

    }
}