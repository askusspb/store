﻿using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    /// <summary>
    /// Регистрация нового пользователя
    /// </summary>
    public class RegisterUserController : BaseStoreController
    {
        private readonly IUserRegistrationService _userRegistrationService;

        public RegisterUserController(IUserRegistrationService registerSrv) 
        {
            _userRegistrationService = registerSrv;
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [JsonException]
        [AjaxOnly]
        public async Task Register(UserRegistrationVm model)
        {
            var user = await _userRegistrationService.RegisterUser(model);
        }
    }
}