﻿using Askus.Store.Attributes;
using Askus.Store.Infrastructure.Domain.Publication;
using Askus.Store.Infrastructure.Interface.Exceptions;
using Askus.Store.Infrastructure.Interface.Repositories;
using Askus.Store.Infrastructure.Interface.Services;
using Askus.Store.Infrastructure.ViewModel;
using Askus.Store.Models.Page;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Askus.Store.Controllers
{
    public class SectionController : BaseStoreController
    {
        private readonly IDomainRepository<Section> _sectionRepository;

        public SectionController(IDomainRepository<Section> domainRepository)
        {
            _sectionRepository = domainRepository;
        }

        public ViewResult Index()
        {
            return View();
        }

        public ViewResult Map(int id)
        {
            var item = FindItem(id, GetMenuItems());
            return View(item);
        }

        [AjaxOnly]
        [JsonException]
        [HttpGet]
        public async Task<JsonResult> GetSectionList(FilterVM filter)
        {
            if (filter == null)
                filter = new FilterVM() { Skip = 0, Take = MaxPageCount };

            var data = await _sectionRepository.GetFilteredAsync(filter);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task SaveSection(Section model)
        {
            if (string.IsNullOrWhiteSpace(model.Title))
                throw new ValidationException("Поле заголовок обязательно!");

            await _sectionRepository.SaveAsync(model);
        }

        [AjaxOnly]
        [JsonException]
        [HttpPost]
        public async Task Remove(int id)
        {
            var page = await _sectionRepository.GetByIdAsync(id);
            await _sectionRepository.RemoveAsync(page);
        }

        private MenuItemModel FindItem(int id, IEnumerable<MenuItemModel> src)
        {
            foreach(var item in src)
            {
                if (item.SectionId == id)
                    return item;
                else if (item.ChildItems != null)
                {
                    var r = FindItem(id, item.ChildItems);
                    if (r != null)
                        return r;
                }
            }
            return null;
        }

    }
}