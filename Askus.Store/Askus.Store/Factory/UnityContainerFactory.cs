﻿using Askus.Store.Infrastructure.Interface.Application;
using Askus.Store.Infrastructure.Interface.Repositories;
using Microsoft.Practices.Unity.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Unity;
using Unity.AspNet.Mvc;

namespace Askus.Store.Factory
{
    public static class UnityContainerFactory
    {
        private static IUnityContainer _unityContainer;

        public static IUnityContainer GetConfiguredContainer()
        {
            //var fileMap = new ExeConfigurationFileMap { ExeConfigFilename = "Web.config" };
            //Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);
            //var unitySection = (UnityConfigurationSection)configuration.GetSection("unity");
            //var container = new UnityContainer().LoadConfiguration(unitySection);

            if (_unityContainer == null)
            {
                _unityContainer = new UnityContainer().LoadConfiguration();
                _unityContainer.RegisterType<IUserContext, UserContext>(new PerRequestLifetimeManager());
            }
            return _unityContainer;
        }
    }
}