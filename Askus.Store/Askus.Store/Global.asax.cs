﻿using Askus.Store.Factory;
using Askus.Store.Infrastructure.Interface.Application;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity;
using Unity.AspNet.Mvc;

namespace Askus.Store
{
    public class MvcApplication : System.Web.HttpApplication
    {
        static MvcApplication()
        {
            // Register our module
            DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }
        
        protected void Application_Start()
        {
            //DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ControllerBuilder.Current.SetControllerFactory(new IocControllerFactory(UnityContainerFactory.GetConfiguredContainer()));
            DependencyResolver.SetResolver(new UnityDependencyResolver(UnityContainerFactory.GetConfiguredContainer()));
        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

            // Get the exception object.
            Exception exc = Server.GetLastError();

            // Handle HTTP errors
            if (exc.GetType() == typeof(HttpException))
            {
                // The Complete Error Handling Example generates
                // some errors using URLs with "NoCatch" in them;
                // ignore these here to simulate what would happen
                // if a global.asax handler were not implemented.
                if (exc.Message.Contains("NoCatch") || exc.Message.Contains("maxUrlLength"))
                    return;
            }

            // Log the exception 
            UnityContainerFactory.GetConfiguredContainer().Resolve<ILogger>().LogException(exc, "Unhandled exception!");

            // Clear the error from the server
            Server.ClearError();
        }
    }
}
