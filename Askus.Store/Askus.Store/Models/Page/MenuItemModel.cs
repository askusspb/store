﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Askus.Store.Models.Page
{
    public class MenuItemModel
    {
        public int? SectionId { get; set; }
        public string Title { get; set; }
        public bool IsSelected { get; set; } = false;
        public string ControllerName { get; set; }
        public string ActionName { get; set; } = "Index";
        public object Params { get; set; } = null;

        public List<MenuItemModel> ChildItems { get; set; } = null;
    }
}