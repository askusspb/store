﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Askus.Store.Models.Page
{
    public class MenuModel
    {
        public List<MenuItemModel> Items { get; set; }

        public bool IsAutorized { get; set; }
        public string UserName { get; set; }
    }
}